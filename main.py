import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import RandomNormal
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Conv2DTranspose
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Concatenate
from matplotlib import pyplot
from tensorflow.keras.layers import LeakyReLU
import tensorflow_addons as tfa
import numpy as np
from random import random

# define the discriminator
def Discriminator(image_shape):
    # weight initialization
    init = RandomNormal(stddev=0.02)
    # source image input
    input = Input(shape=image_shape)
    d = Conv2D(64, (4, 4), strides=(2, 2), padding='same', kernel_initializer=init)(input)
    d = LeakyReLU(alpha=0.2)(d)
    d = Conv2D(128, (4, 4), strides=(2, 2), padding='same', kernel_initializer=init)(d)
    d = tfa.layers.InstanceNormalization(axis=-1)(d)
    d = LeakyReLU(alpha=0.2)(d)
    d = Conv2D(256, (4, 4), strides=(2, 2), padding='same', kernel_initializer=init)(d)
    d = tfa.layers.InstanceNormalization(axis=-1)(d)
    d = LeakyReLU(alpha=0.2)(d)
    d = Conv2D(512, (4, 4), strides=(2, 2), padding='same', kernel_initializer=init)(d)
    d = tfa.layers.InstanceNormalization(axis=-1)(d)
    d = LeakyReLU(alpha=0.2)(d)
    # second last output layer
    d = Conv2D(512, (4, 4), padding='same', kernel_initializer=init)(d)
    d = tfa.layers.InstanceNormalization(axis=-1)(d)
    d = LeakyReLU(alpha=0.2)(d)
    # output
    output = Conv2D(1, (4, 4), padding='same', kernel_initializer=init)(d)
    model = Model(input, output)
    model.compile(loss='mse', optimizer=Adam(lr=0.0002, beta_1=0.5), loss_weights=[0.5])
    return model

# define the residual block
def ResidualBlock(filters, input_layer):
    # weight initialization
    init = RandomNormal(stddev=0.02)
    r = Conv2D(filters, (3, 3), padding='same', kernel_initializer=init)(input_layer)
    r = tfa.layers.InstanceNormalization(axis=-1)(r)
    r = Activation('relu')(r)
    r = Conv2D(filters, (3, 3), padding='same', kernel_initializer=init)(r)
    r = tfa.layers.InstanceNormalization(axis=-1)(r)
    # concatenate merge channel-wise with input layer
    r = Concatenate()([r, input_layer])
    return r

# define the generator
def Generator(image_shape, n_resnet=9):
    # weight initialization
    init = RandomNormal(stddev=0.02)
    input = Input(shape=image_shape)
    g = Conv2D(64, (7, 7), padding='same', kernel_initializer=init)(input)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    g = Activation('relu')(g)
    g = Conv2D(128, (3, 3), strides=(2, 2), padding='same', kernel_initializer=init)(g)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    g = Activation('relu')(g)
    g = Conv2D(256, (3, 3), strides=(2, 2), padding='same', kernel_initializer=init)(g)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    g = Activation('relu')(g)
    for _ in range(n_resnet):
        g = ResidualBlock(256, g)
    g = Conv2DTranspose(128, (3, 3), strides=(2, 2), padding='same', kernel_initializer=init)(g)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    g = Activation('relu')(g)
    g = Conv2DTranspose(64, (3, 3), strides=(2, 2), padding='same', kernel_initializer=init)(g)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    g = Activation('relu')(g)
    g = Conv2D(3, (7, 7), padding='same', kernel_initializer=init)(g)
    g = tfa.layers.InstanceNormalization(axis=-1)(g)
    output = Activation('tanh')(g)
    model = Model(input, output)
    return model


# define a composite model for updating generators by adversarial and cycle loss
def CompositeModel(generator1, discriminator, generator2, image_shape):
    # ensure the model we're updating is trainable
    generator1.trainable = True
    # mark discriminator as not trainable
    discriminator.trainable = False
    # mark other generator model as not trainable
    generator2.trainable = False
    # discriminator element
    input_gen = Input(shape=image_shape)
    gen1_out = generator1(input_gen)  # A ->B
    output_d = discriminator(gen1_out)  # real/fake
    # identity element
    input_id = Input(shape=image_shape)
    output_id = generator1(input_id)  # A ->A
    # forward cycle
    output_f = generator2(gen1_out)  # A ->B ->A
    # backward cycle
    gen2_out = generator2(input_id)  # B ->A
    output_b = generator1(gen2_out)  # B -> A ->B
    # define model
    model = Model([input_gen, input_id], [output_d, output_id, output_f, output_b])
    # define optimization algorithm configuration
    opt = Adam(lr=0.0002, beta_1=0.5)
    # compile model with weighting of least squares loss and L1 loss
    model.compile(loss=['mse', 'mae', 'mae', 'mae'], loss_weights=[1, 5, 10, 10], optimizer=opt)
    return model


# load training dataset
def LoadData(filename):
    data = np.load(filename)
    X1, X2 = data['arr_0'], data['arr_1']
    # scale from [0,255] to [-1,1]
    X1 = (X1 - 127.5) / 127.5
    X2 = (X2 - 127.5) / 127.5
    return [X1, X2]


# select a batch of random samples, returns images and target
def RandomChooseSamples(dataset, samples, patch_shape):
    # choose random instances
    ix = np.random.randint(0, dataset.shape[0], samples)
    # retrieve selected images
    X = dataset[ix]
    # generate 'real' class labels (1)
    y = np.ones((samples, patch_shape, patch_shape, 1))
    return X, y


# generate fake images
def GenerateFakeImages(generator, dataset, patch_shape):
    # generate fake instance
    X = generator.predict(dataset)
    # create 'fake' class labels (0)
    y = np.zeros((len(X), patch_shape, patch_shape, 1))
    return X, y


# save the generator models to file
def SaveModel(step, g_model_AtoB, g_model_BtoA):
    # save the first generator model
    filename1 = 'g_model_AtoB_%06d.h5' % (step + 1)
    g_model_AtoB.save(filename1)
    # save the second generator model
    filename2 = 'g_model_BtoA_%06d.h5' % (step + 1)
    g_model_BtoA.save(filename2)
    print('>Saved: %s and %s' % (filename1, filename2))


# generate samples and save as a plot and save the model
def ShowPerformance(step, g_model, trainX, name, samples=5):
    # select a sample of input images
    X_in, _ = RandomChooseSamples(trainX, samples, 0)
    # generate translated images
    X_out, _ = GenerateFakeImages(g_model, X_in, 0)
    # scale all pixels from [-1,1] to [0,1]
    X_in = (X_in + 1) / 2.0
    X_out = (X_out + 1) / 2.0
    # plot real images
    for i in range(samples):
        pyplot.subplot(2, samples, 1 + i)
        pyplot.axis('off')
        pyplot.imshow(X_in[i])
    # plot translated image
    for i in range(samples):
        pyplot.subplot(2, samples, 1 + samples + i)
        pyplot.axis('off')
        pyplot.imshow(X_out[i])
    # save plot to file
    filename1 = '%s_generated_plot_%06d.png' % (name, (step + 1))
    pyplot.savefig(filename1)
    pyplot.close()


# update image pool for fake images
def UpdateImagePool(pool, images, max_size=50):
    selected = list()
    for image in images:
        if len(pool) < max_size:
            # stock the pool
            pool.append(image)
            selected.append(image)
        elif random() < 0.5:
            # use image, but don't add it to the pool
            selected.append(image)
        else:
            # replace an existing image and use replaced image
            ix = np.random.randint(0, len(pool))
            selected.append(pool[ix])
            pool[ix] = image
    return np.asarray(selected)


# training function
def train(discriminatorA, discriminatorB, generatorA2B, generatorB2A, compositemodelA2B, compositemodelB2A, dataset):
    # define properties of the training run
    epoch, batch, = 85, 1
    # determine the output square shape of the discriminator
    patch = discriminatorA.output_shape[1]  # 16
    # load training data
    trainA, trainB = dataset
    # prepare fake image pool
    poolA, poolB = list(), list()
    # calculate the number of batches per training epoch
    bat_per_epo = int(len(trainA) / batch)
    # calculate the number of training iterations
    steps = bat_per_epo * epoch
    # manually enumerate epochs
    print("batches per training epoch:" + str(bat_per_epo))
    for i in range(steps):
        # select a batch of real samples
        X_realA, y_realA = RandomChooseSamples(trainA, batch, patch)
        X_realB, y_realB = RandomChooseSamples(trainB, batch, patch)
        # generate a batch of fake samples
        X_fakeA, y_fakeA = GenerateFakeImages(generatorB2A, X_realB, patch)  # B -> A
        X_fakeB, y_fakeB = GenerateFakeImages(generatorA2B, X_realA, patch)  # A -> B
        # update fake image pool
        X_fakeA = UpdateImagePool(poolA, X_fakeA)
        X_fakeB = UpdateImagePool(poolB, X_fakeB)
        # update generator B->A via adversarial and cycle loss
        g_loss2, _, _, _, _ = compositemodelB2A.train_on_batch([X_realB, X_realA], [y_realA, X_realA, X_realB, X_realA])
        # update discriminator for A -> real/fake
        dA_loss1 = discriminatorA.train_on_batch(X_realA, y_realA)
        dA_loss2 = discriminatorA.train_on_batch(X_fakeA, y_fakeA)
        # update generator A->B via adversarial and cycle loss
        g_loss1, _, _, _, _ = compositemodelA2B.train_on_batch([X_realA, X_realB], [y_realB, X_realB, X_realA, X_realB])
        # update discriminator for B -> real/fake
        dB_loss1 = discriminatorB.train_on_batch(X_realB, y_realB)
        dB_loss2 = discriminatorB.train_on_batch(X_fakeB, y_fakeB)
        # summarize performance
        loss = '>%d, discriminatorA[%.3f,%.3f] discriminatorB[%.3f,%.3f] generator[%.3f,%.3f]' % (
            i + 1, dA_loss1, dA_loss2, dB_loss1, dB_loss2, g_loss1, g_loss2)
        print(loss)
        # evaluate the model performance and save model every 5 epochs
        if (i + 1) % (bat_per_epo * 5) == 0:
            # plot A->B translation
            ShowPerformance(i, generatorA2B, trainA, 'AtoB')
            # plot B->A translation
            ShowPerformance(i, generatorB2A, trainB, 'BtoA')
            SaveModel(i, generatorA2B, generatorB2A)
        # save the losses every 2 steps
        if (i + 1) % 2 == 0:
            f = open('data/loss.txt', 'a')

            f.write('\n' + loss)

            f.close()


if __name__ == '__main__':
    # load image data
    dataset = LoadData('data/photo2monet_train.npz')
    print('Loaded', dataset[0].shape, dataset[1].shape)
    # define input shape based on the loaded dataset
    image_shape = dataset[0].shape[1:]
    # generator: A -> B
    generatorA2B = Generator(image_shape)
    # generator: B -> A
    generatorB2A = Generator(image_shape)
    # discriminator: A -> real/fake
    discriminatorA = Discriminator(image_shape)
    # discriminator: B -> real/fake
    discriminatorB = Discriminator(image_shape)
    # composite: A -> B -> A, A -> B -> real/fake
    compositemodelA2B = CompositeModel(generatorA2B, discriminatorB, generatorB2A, image_shape)
    # composite: B -> A -> B, B -> A -> real/fake
    compositemodelB2A = CompositeModel(generatorB2A, discriminatorA, generatorA2B, image_shape)
    # train models
    train(discriminatorA, discriminatorB, generatorA2B, generatorB2A, compositemodelA2B, compositemodelB2A, dataset)
