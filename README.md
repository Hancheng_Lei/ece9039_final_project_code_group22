### This is the final project of group22: 

### Converting image into Monet style using cycle-consistent adversarial networks  

#### group members: Yifeng Xie,Siyang Li,Hancheng Lei  

Abstract�The image generating task is an important discussion topic around the world recently. This article will discuss using a machine learning framework, cycle-consistent adversarial network (CycleGAN), and present the training process. The goal is to generate images with Monet style from scratch based on the given training set (real photos). CycleGAN contributed to two neural networks. One is the generative network, and the other is the discriminator network which can work simultaneously to contest each other and dynamically update the model in an unsupervised way. Thus, we can use this method to create 7,000 to 10,000 images in Monet style and evaluate it using Fr�chet Inception Distance (FID). The best FID score for our model is 103.83 when setting the training epoch at 80. Compared to other groups on Kaggle, our result is in the middle of the pack.